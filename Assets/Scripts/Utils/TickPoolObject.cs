using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TickPoolObject : PooledObject
{

    private int totalTicks = 1;
    private int ticks = 1;

    public int Ticks { get => ticks; set => ticks = value; }
    public int TotalTicks { get => totalTicks; set => totalTicks = value; }

    public delegate void TickHandler();
    public event TickHandler Ticked = delegate { };

    public override void InitPooledObject()
    {
        ticks = totalTicks;
    }

    public void Tick()
    {
        Ticks--;
        Ticked();
        if (ticks == 0)
            Reclaim();
    }

}
