using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComponentBase : MonoBehaviour
{
    void Start() { Start_(); }
    protected virtual void Start_() { }
    private void Awake() { Awake_(); }
    protected virtual void Awake_() { }
    private void OnDestroy() { OnDestroy_(); }
    protected virtual void OnDestroy_() { }

    // update functions are not handled like this, because those have an ongoing impact
    // on performance, that can meg measured. This approach is particularly useful for 
    // standardizing the life cycle of components bases
}
