using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class ObjectPool : MonoBehaviour
{
    private Stack<GameObject> pool_;
    private HashSet<GameObject> activePool_;

    public const int InitialPoolSize = 10;
    public GameObject prefab_;

    public int PoolSize
    {
        get { return pool_.Count; }
    }

    public int TotalPoolSize
    {
        get { return pool_.Count + activePool_.Count; }
    }

    public int ActivePoolSize
    {
        get { return activePool_.Count; }
    }

    public delegate void ObjectInitiation(GameObject newObject);

    void Start()
    {
        pool_ = new Stack<GameObject>();
        activePool_ = new HashSet<GameObject>();

        initPool(InitialPoolSize, prefab_);
    }

    public void initPool(int poolSize = 1, GameObject prefab = null)
    {
        if (prefab_ == null)
            prefab = prefab_;
        if (poolSize < 1)
            poolSize = 1;

        for (int i = 0; i < poolSize; i++)
        {
            var newObj = GameObject.Instantiate(prefab_);
            newObj.SetActive(false);
            pool_.Push(newObj);
        }
    }

    public GameObject GetOne(Transform spawnTransform,
        Transform parent,
        ObjectInitiation init)
    {

        if (pool_.Count == 0)
        {
            initPool(ActivePoolSize);
        }

        var result = pool_.Pop();

        var pooled = result.GetComponents<PooledObject>();
        foreach (var item in pooled)
        {
            item.InitPooledObject();
            item.ParentPool = this;
        }

        result.transform.position = spawnTransform.position;
        result.transform.rotation = spawnTransform.rotation;
        result.transform.localScale = spawnTransform.localScale;

        result.transform.parent = parent;

        result.SetActive(true);

        activePool_.Add(result);

        return result;
    }

    public void Reclaim(GameObject obj)
    {
        if (!activePool_.Contains(obj))
        {
            Debug.LogWarning("Invalid object reclaiming: ");
            return;
        }

        activePool_.Remove(obj);
        pool_.Push(obj);

        obj.SetActive(false);
        obj.transform.parent = gameObject.transform;
    }
}