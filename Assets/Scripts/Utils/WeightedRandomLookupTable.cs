﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class WeightedTableEntry {
    public GameObject Entry;
    public int Weight;
}

public class WeightedRandomLookupTable : MonoBehaviour {
    [SerializeField]
    private List<WeightedTableEntry> Entries;
    
    public GameObject Roll () {
        int roll = Random.Range (0, getWeightSum () + 1);
        int currentSum = 0;
        foreach (var item in Entries) {
            currentSum += item.Weight;
            if (currentSum >= roll) {
                return item.Entry.GetComponent<WeightedRandomLookupTable> ()?.Roll () ?? item.Entry;
            }
        }
        return null;
    }

    private int getWeightSum () {
        int result = 0;
        foreach (var item in Entries) {
            result += item.Weight;
        }
        return result;
    }

    [SerializeField]
    public int testNumber = 300;
    public void Test () {
        Dictionary<GameObject, int> result = new Dictionary<GameObject, int> ();
        for (int i = 0; i < testNumber; i++) {
            var e = Roll ();
            if (result.ContainsKey (e))
                result[e] = result[e] + 1;
            else
                result[e] = 1;
        }

        // print out the result
        foreach (var item in result.Keys) {
            Debug.Log (item.name + ": " + result[item]);
        }

    }

}
/* 
[CustomEditor (typeof (WeightedRandomLookupTable))]
public class TableEditor : Editor {
    public override void OnInspectorGUI () {
        base.OnInspectorGUI ();
        var table = target as WeightedRandomLookupTable;

        GUILayout.BeginHorizontal ();
        if (GUILayout.Button ("Roll test"))
            table.Test ();

        GUILayout.EndHorizontal ();

    }
} */