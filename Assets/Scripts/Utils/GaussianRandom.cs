using UnityEngine;

public static class GaussianRandom
{
    private static bool hasSpareValue = false;
    private static float spareValue;

    // Generates a random number from a Gaussian distribution with mean 0 and standard deviation 1
    public static float NextGaussian()
    {
        if (hasSpareValue)
        {
            hasSpareValue = false;
            return spareValue;
        }

        float u, v, s;
        do
        {
            u = 2.0f * Random.value - 1.0f;
            v = 2.0f * Random.value - 1.0f;
            s = u * u + v * v;
        }
        while (s >= 1.0f || s == 0.0f);

        s = Mathf.Sqrt((-2.0f * Mathf.Log(s)) / s);
        spareValue = v * s;
        hasSpareValue = true;
        return u * s;
    }

    // Generates a random number from a Gaussian distribution with a specified mean and standard deviation
    public static float NextGaussian(float mean, float stdDeviation)
    {
        return mean + NextGaussian() * stdDeviation;
    }
}