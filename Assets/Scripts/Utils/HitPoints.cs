using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPoints : MonoBehaviour, IProgress
{
    [SerializeField]
    private float hp;

    public float HP
    {
        get => hp;

        set
        {
            bool isDamaged = hp > value;
            bool isHealed = hp < value;
            bool wasAlive = isAlive;
            hp = value;
            if (hp <= 0)
            {
                hp = 0;
                if (wasAlive)
                    Dead(this);
                return;
            }

            if (hp > maxHP)
            {
                hp = maxHP;
                return;
            }

            if (isDamaged)
                Damaged(this);

            if (isHealed)
                Healed(this);

            HpChanged?.Invoke(this);
            ProgressChanged?.Invoke(Progress());
        }
    }

    public float HPPercentage
    {
        get
        {
            return (float)HP / (float)maxHP;
        }
    }
    public delegate void HPChangeHandler(HitPoints owner);

    public event HPChangeHandler Damaged = delegate { };
    public event HPChangeHandler Healed = delegate { };
    public event HPChangeHandler HpChanged = delegate { };

    public event HPChangeHandler Dead = delegate { };
    public event ProgressChangeHandler ProgressChanged = delegate { };

    public bool isAlive
    {
        get => hp > 0;
    }

    [SerializeField]
    private float maxHP;
    public float MaxHP { get => maxHP; set => maxHP = value; }
    public bool IsHurt { get => isHurt; set => isHurt = value; }
    private bool isHurt;
    private float lastHp;

    public void Damage(float damage)
    {
        HP -= damage;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.X))
            Damage(5 * Time.deltaTime);
    }
    private void LateUpdate()
    {
        isHurt = lastHp != HP;
        lastHp = hp;
    }

    public float Progress()
    {
        return HPPercentage;
    }
}
