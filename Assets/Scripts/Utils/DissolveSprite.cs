using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class DissolveSprite : MonoBehaviour
{
    public Material dissolveMaterial;

    public SpriteRenderer[] sprites;

    public float DissolveTime;
    public float Progress;


    public delegate void DissolveEventHandler();
    public event DissolveEventHandler DissolveStarted = delegate { };
    public event DissolveEventHandler DissolveFinished = delegate { };
    public bool dissolveAtStart = false;
    // Start is called before the first frame update
    void Start()
    {
        sprites = GetComponentsInChildren<SpriteRenderer>();
        dissolving = false;
        if (dissolveAtStart)
        {
            Dissolve();
        }
    }

    public void Dissolve()
    {
        foreach (var sprite in sprites)
        {
            sprite.material = dissolveMaterial;
        }
        dissolving = true;
        Progress = 0;
        DissolveStarted();
    }

    public bool dissolving;
    public float p;

    public bool looping;
    // Update is called once per frame
    void Update()
    {
        if (!dissolving)
            return;

        Progress += Time.deltaTime;

        p = Mathf.Lerp(1, 0.5f, Progress / DissolveTime);
        foreach (var sprite in sprites)
            sprite.material.SetFloat("_DissolvePower", p);

        if (Progress > DissolveTime)
        {
            DissolveFinished();
            if (looping)
            {
                Progress = 0;
                DissolveStarted();
            }
            else
            {
                dissolving = false;
            }
        }
    }
}
