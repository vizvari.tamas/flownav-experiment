using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifetimePoolObject : PooledObject
{
    public float totalLifeTime = 5;
    float lifetime = 5;
    
    public override void InitPooledObject() {
        lifetime = totalLifeTime;
    }

    // Update is called once per frame
    void Update()
    {
        lifetime -= Time.deltaTime;
        if (lifetime < 0)
            Reclaim();
    }
}
