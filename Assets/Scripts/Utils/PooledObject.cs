using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Base class for the Pooled object components
public class PooledObject : MonoBehaviour
{
    public ObjectPool ParentPool;
    public virtual void InitPooledObject()
    {
        // it should do something on init
    }

    public virtual void Reclaim()
    {
        if (ParentPool == null)
        {
            Debug.LogWarning("Pool object without a parent: " + name);
            return;
        }
        ParentPool.Reclaim(gameObject);
    }
}
