﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TickHandlerBase : MonoBehaviour, ITickHandler
{

    public void Init()
    {
        TickSource.AfterTick += OnTick;
    }
    public void Deinit()
    {
        TickSource.AfterTick -= OnTick;
    }

    public abstract void OnTick(object initiator, int stepCount);

    // Use this for initialization
    void Start()
    {
        Init();
    }
    /// <summary>
    /// This function is called when the MonoBehaviour will be destroyed.
    /// </summary>
    void OnDestroy()
    {
        Deinit();
    }
}
