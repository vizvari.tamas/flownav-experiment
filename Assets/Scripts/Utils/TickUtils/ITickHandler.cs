﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITickHandler
{
    void Init();
	
    void Deinit();

    void OnTick(object initiator, int stepCount);
}
