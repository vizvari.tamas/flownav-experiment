﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TickSource : Singleton<TickSource>
{

    public int StepCount { get; set; }

    public static int GetStepCount()
    {
        return Instance.StepCount;
    }

    public delegate void ResetHandler();
    public static event ResetHandler TickSourceReset = delegate { };
    public static void Reset()
    {
        Instance.StepCount = 0;
        TickSourceReset();
    }

    public delegate void TickHandler(object initiator, int stepCount);
    public static event TickHandler BeforeTick = delegate { };
    public static event TickHandler AfterTick = delegate { };

    public static void InitiateTick(object initiator)
    {
        BeforeTick(initiator, GetStepCount() + 1);
        Instance.StepCount++;
        AfterTick(initiator, GetStepCount());
    }
}
