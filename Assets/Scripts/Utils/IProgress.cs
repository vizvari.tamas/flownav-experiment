using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void ProgressChangeHandler(float progressPercentage);

public interface IProgress
{
    public float Progress();

    public event ProgressChangeHandler ProgressChanged;

}
