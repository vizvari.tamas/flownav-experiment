using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavTextureHelper : MonoBehaviour {
    [SerializeField] Texture2D heatmap;

    [SerializeField] Vector3 directionVector;

    [SerializeField] float scale;

    [SerializeField] float speed;
    [SerializeField] Vector2Int texpos;

    [SerializeField][Range(0, 512)] float offsetX;
    [SerializeField][Range(0, 512)] float offsetY;

    [SerializeField] Vector2Int gizmoResolution;

    [SerializeField] Color rightPixel;
    [SerializeField] Color leftPixel;
    [SerializeField] Color upPixel;
    [SerializeField] Color downPixel;
    [SerializeField] Vector2 pos2;


    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    Vector2Int GetTexPosition(int x, int z) {
        var pos = new Vector2(x, z);
        var texpos = new Vector2Int(Mathf.RoundToInt(pos.x * scale + offsetX), Mathf.RoundToInt(pos.y * scale + offsetY));
        return texpos;
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.blue;
        for (int x = 0; x < gizmoResolution.x; x++) {
            for (int z = 0; z < gizmoResolution.x; z++) {
                var basePoint = new Vector3(x, 0, z) * scale;
                pos2 = new Vector2(x, z);
                texpos = new Vector2Int(Mathf.RoundToInt(pos2.x * scale + offsetX), Mathf.RoundToInt(pos2.y * scale + offsetY));
                var tip = basePoint + Vector3.up * GetHeight(texpos.x, texpos.y);
                Gizmos.DrawLine(basePoint, tip);
            }
        }
    }

    private float GetHeight(int x, int y) {
        Texture2D texture = heatmap;
        int width = texture.width;
        int height = texture.height;

        // Ensure the position is within the texture boundaries.
        x = Mathf.Clamp(x, 0, width - 1);
        y = Mathf.Clamp(y, 0, height - 1);

        return texture.GetPixel(x, y).r;
    }

    [SerializeField] int roughness;
    private Vector3 GetTextureGradientAtPosition(int x, int y) {
        Texture2D texture = heatmap;
        int width = texture.width;
        int height = texture.height;

        // Ensure the position is within the texture boundaries.
        x = Mathf.Clamp(x, 0, width - 1);
        y = Mathf.Clamp(y, 0, height - 1);

        // Calculate the gradient using central differences (approximation).
        rightPixel = texture.GetPixel(x + roughness, y);
        leftPixel = texture.GetPixel(x - roughness, y);
        upPixel = texture.GetPixel(x, y + roughness);
        downPixel = texture.GetPixel(x, y - roughness);

        // Calculate the gradient along the x and y axes.
        float gradientX = (rightPixel.r - leftPixel.r + rightPixel.g - leftPixel.g + rightPixel.b - leftPixel.b) * 0.5f;
        float gradientY = (upPixel.r - downPixel.r + upPixel.g - downPixel.g + upPixel.b - downPixel.b) * 0.5f;

        return new Vector3(gradientX, 0, gradientY);
    }
}
