using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomScaler : MonoBehaviour {

    [SerializeField] Vector3 fromScale;
    [SerializeField] Vector3 toScale;

    [SerializeField] float cycle;
    [SerializeField] float time;

    // Start is called before the first frame update
    void Start() {
        time = Random.Range(0, cycle);
        if (Random.value < .5f) {
            var tmp = fromScale;
            fromScale = toScale;
            toScale = tmp;
        }
    }

    // Update is called once per frame
    void Update() {
        if (time < cycle)
            time += Time.deltaTime;
        else {
            var tmp = fromScale;
            fromScale = toScale;
            toScale = tmp;
            time = 0;
        }
        transform.localScale = Vector3.Lerp(fromScale, toScale, time / cycle);
    }
}
