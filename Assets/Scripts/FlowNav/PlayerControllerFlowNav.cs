using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerFlowNav : MonoBehaviour {

    Rigidbody2D body;

    [SerializeField] float speed;

    // Start is called before the first frame update
    void Start() {
        body = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {
        var control = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        body.velocity = control * speed;
    }
}
