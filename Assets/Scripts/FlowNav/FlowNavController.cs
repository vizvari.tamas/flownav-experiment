using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowNavController : MonoBehaviour {

    [SerializeField] Texture2D heatmap;

    [SerializeField] Vector3 directionVector;

    [SerializeField] float scale;

    [SerializeField] float speed;
    [SerializeField] Vector2Int texpos;

    [SerializeField][Range(0, 512)] float offsetX;
    [SerializeField][Range(0, 512)] float offsetY;
    // Start is called before the first frame update
    void Start() {
        body = GetComponent<Rigidbody2D>();
    }
    [SerializeField] Color rightPixel;
    [SerializeField] Color leftPixel;
    [SerializeField] Color upPixel;
    [SerializeField] Color downPixel;
    [SerializeField] Vector2 pos2;

    Rigidbody2D body;
    // Update is called once per frame
    void Update() {
        // characters should move from high heat places to lower heat places
        pos2 = new Vector2(transform.position.x, transform.position.y);
        texpos = new Vector2Int(Mathf.RoundToInt(pos2.x * scale + offsetX), Mathf.RoundToInt(pos2.y * scale + offsetY));
        var directionVector = -GetTextureGradientAtPosition(texpos.x, texpos.y).normalized * speed;
        body.velocity = directionVector;
    }

    [SerializeField] int roughness;
    private Vector3 GetTextureGradientAtPosition(int x, int y) {
        Texture2D texture = heatmap;
        int width = texture.width;
        int height = texture.height;

        // Ensure the position is within the texture boundaries.
        x = Mathf.Clamp(x, 0, width - 1);
        y = Mathf.Clamp(y, 0, height - 1);

        // Calculate the gradient using central differences (approximation).
        rightPixel = texture.GetPixel(x + roughness, y);
        leftPixel = texture.GetPixel(x - roughness, y);
        upPixel = texture.GetPixel(x, y + roughness);
        downPixel = texture.GetPixel(x, y - roughness);

        // Calculate the gradient along the x and y axes.
        float gradientX = (rightPixel.r - leftPixel.r + rightPixel.g - leftPixel.g + rightPixel.b - leftPixel.b) * 0.5f;
        float gradientY = (upPixel.r - downPixel.r + upPixel.g - downPixel.g + upPixel.b - downPixel.b) * 0.5f;

        return new Vector3(gradientX, gradientY, 0);
    }
}
