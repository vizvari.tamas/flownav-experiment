using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class FlowNavController3d : MonoBehaviour {

    [SerializeField] Texture2D heatmap;

    [SerializeField] Vector3 directionVector;

    [SerializeField] float scale;

    [SerializeField] float speed;
    [SerializeField] Vector2Int texpos;

    [SerializeField][Range(0, 512)] float offsetX;
    [SerializeField][Range(0, 512)] float offsetY;
    // Start is called before the first frame update
    void Start() {
        body = GetComponent<Rigidbody>();
    }
    [SerializeField] Color rightPixel;
    [SerializeField] Color leftPixel;
    [SerializeField] Color upPixel;
    [SerializeField] Color downPixel;
    [SerializeField] Vector2 pos2;

    Rigidbody body;
    // Update is called once per frame
    void Update() {
        // characters should move from high heat places to lower heat places
        var directionVector = -GetTextureGradientAtPosition(GetTexPosition(transform.position.x, transform.position.z)).normalized * speed;
        body.velocity = directionVector;
        transform.LookAt(transform.position + directionVector * 100);
    }

    [SerializeField] int roughness;
    private Vector3 GetTextureGradientAtPosition(Vector2Int pos) {
        return GetTextureGradientAtPosition(pos.x, pos.y);
    }

    private Vector3 GetTextureGradientAtPosition(int x, int y) {
        Texture2D texture = heatmap;
        int width = texture.width;
        int height = texture.height;

        // Ensure the position is within the texture boundaries.
        x = Mathf.Clamp(x, 0, width - 1);
        y = Mathf.Clamp(y, 0, height - 1);

        // Calculate the gradient using central differences (approximation).
        rightPixel = texture.GetPixel(x + roughness, y);
        leftPixel = texture.GetPixel(x - roughness, y);
        upPixel = texture.GetPixel(x, y + roughness);
        downPixel = texture.GetPixel(x, y - roughness);

        // Calculate the gradient along the x and y axes.
        float gradientX = (rightPixel.r - leftPixel.r + rightPixel.g - leftPixel.g + rightPixel.b - leftPixel.b) * 0.5f;
        float gradientY = (upPixel.r - downPixel.r + upPixel.g - downPixel.g + upPixel.b - downPixel.b) * 0.5f;

        return new Vector3(gradientX, 0, gradientY);
    }

    [SerializeField] Vector2Int gizmoResolution;

    Vector2Int GetTexPosition(float x, float z) {
        var pos = new Vector2(x, z);
        var texpos = new Vector2Int(
            Mathf.RoundToInt(pos.x * scale + offsetX),
            Mathf.RoundToInt(pos.y * scale + offsetY));
        return texpos;
    }

    private void OnDrawGizmosSelected() {
        for (int gx = 0; gx < gizmoResolution.x; gx++) {
            int x = heatmap.width / gizmoResolution.x * gx;
            for (int gz = 0; gz < gizmoResolution.y; gz++) {
                int z = heatmap.height / gizmoResolution.y * gz;
                var basePoint = new Vector3(x, 0, z) * scale;
                var h = GetHeight(GetTexPosition(x, z));
                var tip = basePoint + Vector3.up * h;
                Gizmos.color = Color.Lerp(Color.green, Color.red, h);
                Gizmos.DrawCube(basePoint, new Vector3(.4f, h * 5f, .4f));
            }
        }
    }
    private float GetHeight(Vector2Int pos) {
        return GetHeight(pos.x, pos.y);
    }

    private float GetHeight(int x, int y) {
        Texture2D texture = heatmap;
        int width = texture.width;
        int height = texture.height;

        // Ensure the position is within the texture boundaries.
        x = Mathf.Clamp(x, 0, width - 1);
        y = Mathf.Clamp(y, 0, height - 1);

        return texture.GetPixel(x, y).r;
    }
}
