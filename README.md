# Fon unaffilitaed visitors

Hi visitor, 

This is a public project, but it is not intended to be used outside some experimentation with a friend. It is most likely useless to you, and also it is very messy. Sorry. If for some reason you find something useful, by all means, take it. No merge requests tho. Thank you!

# For Gergő

Innen tudod letölteni a kísárletet amit eddig összedobtam.
Most a 2d-s ott tart ahol mutattam. Sajnos a 3d-s verziót még nem sikerült rendesen megcsinálni, de ha tudok akkor majd foglalkozok vele.

Ha szeretnél rendesen git-et használni - hosszabb tábvon állati hasznos, már csak azért is mert könnyebb megosztani a dolokat, és visszaállni egy korábbi állapotra, ha esetleg valami elromlik, akkor majd megtanítalak, és használhatunk nem publikus repot is, de most ez a legyeszerűbben hozzáférhető 

Addig is, használhatod a zip letöltés funkciót, amit a nagyobbacska kék "Clone" gomb mellett találsz. 

A projekt most éppen 2022.3.7f1 verzióra van lefordítva nálam, de elvileg mással is mennie kellene. 

Találsz a "Scenes" mappában 2 scene-t az egyik a 2D teszt. Ha a Reds - objektum alatt lévő agentek bármelyikét megnézed, van rajtuk egy FlowNavController nevű komponens. Ezeknek a paramétereit lehet piszkálni, és individuálisan át lehet állítani a következőket:


Az alábbi paraméterk azok, amiket érdemes lehet megérteni piszkálni. a több csak debufra való és production-ben már nem is tenném itt láthatóvá:

* Heatmap: Egy textúra ami alapján a navigáció törtéenik. Heatmapnek nevezem, mert ha meghagyod szürkeárnyalatosnak, akkor a magas (világos) részeken high heat van, attól fognak óvakodni az ágensek, tehát alapjában véve be fognak mozogni a sötétebb vermekbe. A jelenet hátterében lévő textúra csak indikáció, a valódi szimulációra nincsne hatással, minden ágensnek lehet akár teljesen saját heatmapje, ami dinamikusan változhat is. Ide érdemes lesz betenned a saját textúrádat, ha meg akarod nézni hogy az hogy működne.
* Scale - arra jó hogy a textúra pixelek mekkora valós térbeli területnek felelnek meg, nem érdemes állítgatni hacsak nem különböző felbontású textúrát szeretnél használni különböző agenteken.
* Speed - a bogyó mozgási sebessége. Érdekes lehet, most nem állítja semmi, de ha kézzel állítgatod tudod vizsgálni milyen lenne
* Offset - ezzekkel el tudod tolni hogy a valós pozició melyik textúra koordinátának felel meg. Ezt érdemes lehet menet közben tologatni, hogy kipróbálhasd hogy reagálnak a textúra változásra a kis NPC-k
* Roughness - ez mondja meg, hogy mekkora távolságról vegyen mintát a környezetéből az NPC hogy meghatározza, merre menjen. Ha túl kicsit, akkor miden kis "gödörbe" beragad, ha túl nagy, akkor viszon kisebb lokális dolgokat át is ugrik. Dinamikus változtatásával ki lelhet mozditani lokális minimumból egyes bogyókat.

A RandomScaler komponens változtatja bizonyos keretek között a méreteiket, ha gondolod próbálgathatod, de ignorálhatod vagy ki is kapcsolhatod őket. 

A kék pötty egy irányított karakter, egyelőre a textúrára nem hat, csak fizikailag el tudja tologatni az egyes NPC-ket.

A másik jelenet a 3d lesz, de az még nem műklödik jól, úgyhogy nem is írom le mi hogy van benne.


# Fejlesztés következő lépései

Most szeretném először megcsinálni hogy a 3D-s jelenet legalább olyan szinten menjen mint a 3D-s sík terpen. Utána megbeszéljük mi lehet még érdekes. 

szerintem a következő az hogy a játékos tudjon zavart okozni, azaz dinamikusan változtatni a heatmap-en, mindjuk egy lövéssel, ami csinálna egy nem kívánatos zónát maga vagy a találat helye körül, ilyesmi.